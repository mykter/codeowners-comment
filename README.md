# codeowners-comment

This file has two code owners, and there's no indication that there's a problem with the CODEOWNERS file.

Anyone reading the CODEOWNERS file would expect only @mykter to be a codeowner!
